/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testservice;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author PONG
 */
public class testcustomerservice {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer :cs.getCustomers()){
             System.out.println(customer);
        }
        System.out.println(cs.getByTel("0888868888"));
        Customer cusl = new Customer("Peach", "0934781717");
        cs.addNew(cusl);
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0934781717");
        delCus.setTel("0934781717");
        cs.update(delCus);
        System.out.println("After Update");
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        cs.delete(delCus);
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
    }
}
